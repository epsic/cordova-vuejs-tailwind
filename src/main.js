// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import VueRouter from 'vue-router'
import VueI18n from 'vue-i18n'
import en from './lang/en'
import fr from './lang/fr'

import {routes} from './router'

import './assets/styles.css'

Vue.use(VueRouter)
Vue.use(VueI18n)

const router = new VueRouter({
  mode: 'history',
  routes
})

const i18n = new VueI18n({
  locale: 'fr',
  messages: {
    en,
    fr
  }
})

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  components: {App},
  template: '<App/>',
  router,
  i18n
})
